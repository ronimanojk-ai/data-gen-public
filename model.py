from sqlalchemy import Column, Integer, String, ForeignKey, Table
from sqlalchemy import String, Integer, JSON, SmallInteger, BigInteger, Boolean, Date, DateTime, Float, Numeric, Interval
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

order_item_rel = Table(
    "order_item_rel",
    Base.metadata,
    Column("order_id", Integer, ForeignKey("order.id")),
    Column("order_item_id", Integer, ForeignKey("order_item.id")),
)

order_otif_rel = Table(
    "order_otif_rel",
    Base.metadata,
    Column("order_id", Integer, ForeignKey("order.id")),
    Column("order_otif_id", Integer, ForeignKey("order_otif.id")),
)

item_deliv_rel = Table(
    "item_deliv_rel",
    Base.metadata,
    Column("order_item_id", Integer, ForeignKey("order_item.id")),
    Column("item_deliv_id", Integer, ForeignKey("order_item_deliv_loc.id")),
)

deliv_outlet_rel = Table(
    "deliv_outlet_rel",
    Base.metadata,
    Column("item_deliv_id", Integer, ForeignKey("order_item_deliv_loc.id")),
    Column("item_outlet_id", Integer, ForeignKey("cust_outlet.id")),
)

class Order(Base):
    __tablename__ = "order"
    __table_args__ = {'comment': 'The customer order'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    a_cd = Column(String, unique=True, nullable=False,
                  comment="ACE Generated Order Identifier - created as 'A'+9-digit 'id' column left padded. Eg: A00000235")
    date = Column(DateTime(timezone=True), comment="order arrival date")
    lead_time = Column(Interval, comment="number of days")
    ship_from = Column(String, comment="'DC' or 'MILL'")
    po_num = Column(String, nullable=False, unique=True, comment="Purchase Order Number - will be -1 for Transfer Shipments")
    move_type = Column(String, comment="move_type")
    dc_id = Column(Integer, ForeignKey('dc.id'), comment="actual DC name")
    mill_id = Column(Integer, ForeignKey('mill.id'), comment="actual Mill name")
    status = Column(String, comment="has this order been addressed")
    customer_id = Column(Integer, ForeignKey('customer.id'), comment="the customer for this order")
    items = relationship("OrderItem", backref="order")  # product items contained by this order
    sales_doc_num = Column(String, nullable=False, unique=True, comment="Sales Document Number")
    delivery_type = Column(String, nullable=False, comment="Order delivery type - CPU/FRA")
    req_del_date = Column(DateTime(timezone=True), nullable=False, comment="Requested Order Delivery Date")
    act_del_date = Column(DateTime(timezone=True), comment="Actual Order Delivery Date")
    req_ship_date = Column(DateTime(timezone=True), nullable=False, comment="Requested Order Ship Date")
    act_ship_date = Column(DateTime(timezone=True), comment="Actual Order Ship Date")
    req_iss_date = Column(DateTime(timezone=True), nullable=False, comment="Requested Order Issue Date")
    act_iss_date = Column(DateTime(timezone=True), comment="Actual Order Issue Date")
    shipping_type_desc = Column(String, nullable=False, comment="Shipping Type Description")
    ace_process_max_time = Column(DateTime(timezone=True),
                                  comment="The maximum time until with ACE processing will wait for human order clearance,before auto relsoving and sending to SAP. This is the requested issue date minus estimated time the order might spendd in SAP blocks")
    is_ord_locked = Column(Boolean, nullable=False,
                           comment="Is Order Locked captures if an order is locked from application; and is not available for grooming by Optimizer - either when the order is being checked by user from application / saved specifically by user to come back later")
    interim_save_json = Column(JSON, comment="Interim Save JSON")
    shipments = relationship("OrderShipmentDetail", backref="order")


class OrderItem(Base):
    __tablename__ = "order_item"
    __table_args__ = {'comment': 'Product order items in a customer order'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    sales_doc_item_num = Column(String, nullable=False, comment="sales_doc_item_num")
    order_id = Column(Integer, ForeignKey(f'order.id'), nullable=False, comment="Order Identifier, parent order")
    req_sku = Column(Integer, ForeignKey(f'product.id'), nullable=False, comment="Requested SKU")
    tot_req_quantity = Column(Integer, nullable=False, comment="Requested Quantity")
    req_uom = Column(Integer, ForeignKey(f'uom.id'), nullable=False, comment="Requested UOM")
    status = Column(String, nullable=False, comment="Order Item Status")
    order_item_deliv_locs = relationship("OrderItemDelivLoc", backref="order_item")
    shipto = relationship("OrderItemShipto", backref="order_item")
    #order_item_fulfillments = relationship("OrderItemFulfillment", backref="order_item")
    #order_item_financials = relationship("OrderFinancials", backref="order_item")

class OrderShipmentDetail(Base):

    __tablename__ = "order_shipment_detail"
    #__table_args__ = {"comment": "Allocation of order item fulfilment quantity to multiple ship to locations"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    ord_id = Column(Integer, ForeignKey("order.id"), nullable=False, comment="Order Identifier")
    ship_type = Column(String, nullable=False, comment="Shipment Type - Eg: Intermodal/LTL/Truck etc")
    req_gross_wt = Column(Float, comment="Gross Weight from the requested order")
    req_net_wt = Column(Float, comment="Net Weight from the requested order")
    act_gross_wt = Column(Float, comment="Gross Weight from the actual filled order")
    act_net_wt = Column(Float, comment="Net Weight from the actual filled order")
    wt_uom = Column(Integer, ForeignKey("uom.id"), comment="Weight UOM")


class OrderOTIF(Base):
    __tablename__ = "order_otif"
    __table_args__ = {'comment': 'OTIF details for a given order'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    ord_id = Column(Integer, ForeignKey(f'order.id'), comment='Order Identifier')
    on_time = Column(Float,comment="On-Time")
    fill_rate = Column(Float, comment="Case Fill Rate")
    otif = Column(Float, comment="OTIF Value")

class OrderItemDelivLoc(Base):
    __tablename__ = "order_item_deliv_loc"
    __table_args__={'comment': "To capture the multiple shipto's associated with a requested order"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    ord_item_id = Column(Integer, ForeignKey('order_item.id'), nullable=False, comment="Order Item Identifier")
    is_cpu = Column(Boolean, nullable=False, comment="Is CPU Order = FALSE if Order is FRA; else TRUE")
    customer_outlet = Column(Integer, ForeignKey('cust_outlet.id'), comment="Customer Outlet Identifier= NULL if is_cpu is TRUE; else must be a customer outlet id")
    req_quantity = Column(Integer, nullable=False, comment="Requested Quantity")
    cust_outlet = relationship("CustomerOutlet")

class CustomerOutlet(Base):
    __tablename__ = "cust_outlet"
    __table_args__ = {"comment": "Customer Outlets"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False,  comment="Name")
    loc_id = Column(Integer,  ForeignKey(f'location.id'), nullable=False,comment="Location Identifier")
    cust_id = Column(Integer,  ForeignKey(f'customer.id'), nullable=False,comment="Customer Identifier")
    is_pref_outlet = Column(Boolean,  comment="Is preferred Outlet for Customer")
    ext_shipto_id = Column(String, nullable=True, comment="External Customer Outlet Identifier")
    is_dtc = Column(String, nullable=False, comment="If the cust outlet is direct to customer")
    location = relationship("Location")

class Location(Base):
    __tablename__ = "location"
    __table_args__= {'comment': "Location primary table"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    addr_line_1 = Column(String, nullable=False, comment="Address Line 1 for the address")
    addr_line_2 = Column(String, nullable=True, comment="Address Line 2 for the address (H.No, Apt No etc)")
    city = Column(String, nullable=False, comment="City Name")
    state = Column(String, nullable=False, comment="State Name")
    cntry = Column(String, nullable=False, comment="Country Name")
    city_cd = Column(String, nullable=True, comment="City Code")
    state_cd = Column(String, nullable=True, comment="State Code")
    cntry_cd = Column(String, nullable=True, comment="Country Code")
    zip = Column(String, nullable=False, comment="Zip Code / Pin Code")

class DC(Base):
    __tablename__ = "dc"
    __table_args__ = {"comment": "DC primary table"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    dc_id = Column(String, nullable=False, unique=True, comment="Unique ID for each DC")
    name = Column(String,  nullable=False, comment="DC Name")
    loc_id = Column(Integer, ForeignKey(f'location.id'), comment= "Location Identifier", nullable=False)
    is_rdc = Column(Boolean, comment="Is the DC also an RDC")
    is_active = Column(Boolean, comment="is Active (Default - TRUE)", default=True)
    location = relationship("Location")
    orders = relationship("Order", backref="dc")
    ship_capacity = relationship("DCShipCapacity", backref="dc")

class Mill(Base):

    __tablename__ = "mill"
    __table_args__ = {'comment': "Mil primary table"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String,  nullable=False, comment="Mill Name")
    mill_id = Column(String, unique=True, comment="Unique ID for mill")
    loc_id = Column(Integer, ForeignKey(f'location.id'), comment= "Location Identifier", nullable=False)
    is_active = Column(Boolean, comment="is Active (Default - TRUE)", default=True)
    location = relationship("Location")
    orders = relationship("Order", backref="mill")


class Customer(Base):

    __tablename__ = "customer"
    __table_args__= {"comment":  "Customer primary table"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String,  nullable=False, comment="name of the customer")
    #customer_id = Column(String, unique=True, comment="id of a customer")
    company_desc = Column(String, nullable=False, comment="Name of the company")
    loc_id = Column(Integer, ForeignKey(f'location.id'), comment= "Location Identifier", nullable=False)
    ext_soldto_id = Column(String, comment="External Customer Identifier")
    is_internal_company = Column(Boolean, nullable=False,
                                 comment="Is Internal Company - The value will be False for usual companies; but True for companies which are under Koch/GP hierarchy")
    is_active = Column(Boolean, comment="is Active (Default - TRUE)", default=True)
    orders = relationship("Order", backref="customer")
    cust_outlets = relationship("CustomerOutlet", backref="customer")
    #customer_penalties = relationship("CustPenalties", backref='customer')

class Product(Base):

    __tablename__ = "product"
    __table_args__ = {'comment': "Product primary table"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    prd_cd = Column(String,  nullable=False, unique=True, comment="SKU - Product Code (Identifier)")
    prd_grp_id = Column(Integer, ForeignKey("product_grp.id"), nullable=False, comment="Product Group")
    each_per_ship_unit = Column(Integer, nullable=False, comment="Number of smallest units of product in one ship unit")
    total_sm_per_ship_unit = Column(Integer, nullable=False,
                                    comment="Total of smallest units of product in each ship unit")
    stat_factor = Column(Float, nullable=False,
                         comment="Stat factor used to convert CASE/QSU to Stat units. Quantity(Stat) = Quantity(Case/QSU) * Stat Factor")
    name = Column(String, nullable=False,  comment="Product Name")
    is_active = Column(Boolean, nullable=False, comment="Material Status")
    lev4_desc = Column(String, comment="Level 4 Product Description")
    lev3_desc = Column(String, comment="Level 3 Product Description")
    lev2_desc = Column(String, comment="Level 2 Product Description")
    lev1_desc = Column(String, comment="Level 1 Product Description")
    brand_id = Column(Integer, ForeignKey(f'brand.id'),comment="Product Brand")
    category_id = Column(Integer, ForeignKey('category.id'), comment="Category ID")
    buy_multiple  = Column(Integer, comment="Min buy Multiple of the product")
    buy_multiple_uom = Column(Integer, ForeignKey(f'uom.id'),  comment="Unit of measure for the buy multiple")
    num_sm_uom = Column(Integer,  comment="Number of smallest units in one unit of product")
    sm_uom = Column(Integer, ForeignKey(f'uom.id'),  comment="Unit of measure for the buy multiple")
    length = Column(Float, comment="Product Length")
    width = Column(Float, comment="Product Width")
    height = Column(Float, comment="Product Height")
    len_uom = Column(Integer, ForeignKey(f'uom.id'),  comment="UOM for one dimensional length - Length, Width and Height")
    num_buy_uom_per_pallet = Column(Integer, comment="Number of Product Buy Multiple UOMs(Cases/QSU) in a Pallet")
    net_wt = Column(Float, comment="Product Weight")
    gross_wt = Column(Float, comment="Product  Gross Weight")
    wt_uom = Column(Integer, ForeignKey(f'uom.id'),  comment="UOM for product weight")
    vol = Column(Float, comment="Product Volume")
    vol_uom = Column(Integer, ForeignKey(f'uom.id'),  comment="UOM for product volume")
    order_items = relationship("OrderItem", backref='product')

class UOM(Base):

    __tablename__ = "uom"
    __table_args__ = {'comment': "Unit of Measures"}
    id = Column(Integer, primary_key=True, autoincrement=True)
    code = Column(String, comment="UOM Code - Eg: Cs, Kg, m")
    name = Column(String, nullable=False, comment="UOM Name - Eg: Case, Kilo gram, Meter")
    cat = Column(String, comment="UOM Category - Eg:Quantity, Weight, Distance, Volume, Area etc")
    order_items = relationship("OrderItem", backref='UOM')

class DCProductStock(Base):

    __tablename__ = "dc_product_stock"
    __table_args__={'comment': 'the inventory level for each DC for each product at a given time'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    dc_product_id = Column(Integer, ForeignKey(f'dc_product.id'), nullable=False, comment="DC-Product_Identifier")
    ord_item_id = Column(Integer,  ForeignKey('order_item.id'),
                         comment="Order Item Identifier which represents the activity regarding the actual change in stock")
    pln_avail_dt = Column(DateTime(timezone=True), comment="With respect to stock change for a product in a DC, Planned Date of the activity (Mill transfer, stock transfer, order fulfilment etc)")
    pln_avail_qty = Column(Integer, comment="With respect to stock change for a product in a DC, Planned Quantity of the activity (Mill transfer, stock transfer, order fulfilment etc). This can be positive or negative based on if the quantity is increasing or reducing")
    pln_avail_uom = Column(Integer, ForeignKey(f'uom.id'), comment="With respect to stock change for a product in a DC, UOM for the Planned quantity for the activity (Mill transfer, stock transfer, order fulfilment etc)")
    pln_comment = Column(String, comment="Comment regarding the planned activity")
    act_avail_dt = Column(DateTime(timezone=True), comment="Actual availability Date")
    act_avail_qty = Column(Integer, comment="Actual availability quantity")
    act_avail_uom = Column(Integer, ForeignKey(f'uom.id'), comment="Actual availability UOM")
    act_comment = Column(String, comment="Comment regarding the actual activity")


class DCProduct(Base):
    __tablename__ = "dc_product"
    __table_args__= {'comment' : 'DC that can process specific products'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    dc_id = Column(Integer, ForeignKey(f'dc.id'), nullable=False, comment="DC Identifier")
    prd_id = Column(Integer, ForeignKey(f'product.id'), nullable=False, comment="Product Identifier")
    avg_inc_qty_per_day = Column(Float, comment="Average Incoming Product Quantity Per Day")
    avg_inc_qty_per_day_uom = Column(Integer, ForeignKey(f'uom.id'),comment="Average Incoming Product Quantity Per Day UOM")
    avg_out_qty_per_day = Column(Float, comment="Average Outgoing Product Quantity Per Day")
    avg_out_qty_per_day_uom = Column(Integer, ForeignKey(f'uom.id'), comment="Average Outgoing Product Quantity Per Day UOM")
    safety_stock_level = Column(Float, comment="Safety Stock Level")
    safety_stock_level_uom = Column(Integer, ForeignKey("uom.id"), comment="Safety Stock Level UOM")
    safety_stock_level_days = Column(Float, comment="Target Days for the corresponding Material")
    dc_product_stock = relationship("DCProductStock")
    dcs = relationship("DC", backref="products")
    products = relationship("Product", backref="DCs")


class DCShipCapacity(Base):
    __tablename__ = "dc_ship_capacity"
    # __table_args__ = {'comment': 'Product Brand'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    dc_id = Column(Integer, ForeignKey('dc.id'), nullable=False, comment="DC Identifier")
    max_capacity = Column(Float, nullable=False, comment="Maximum Shipping Capacity")
    capacity_uom = Column(Integer, ForeignKey('uom.id'),nullable=False, comment="Capacity UOM")
    date = Column(DateTime(timezone=True), nullable=False, comment="Date for Capacity")


class DCProductPrice(Base):

    __tablename__ = "dc_product_price"
    #__table_args__= {'comment': 'The price of the product at a given DC'}
    id = Column(Integer, primary_key=True, autoincrement=True)
    dc_product_id = Column(Integer, ForeignKey(f'dc_product.id'), nullable=False, comment="DC Identifier")
    mth_id = Column(Integer, ForeignKey(f'month.id'), nullable=False, comment="Month Identifier")
    price = Column(Float, nullable=False, comment="Base Price of the product in a DC")
    price_uom = Column(Integer, ForeignKey(f'uom.id'), nullable=False, comment="Price UOM")

class OrderItemShipto(Base):
    __tablename__ = "order_item_shipto"
    # __table_args__ = {'comment': 'Product Brand'}

    id = Column(Integer, primary_key=True, autoincrement=True)
    ord_item_id = Column(Integer, ForeignKey('order_item.id'),nullable=False, comment="Order Item Identifier")
    req_quantity = Column(Float, nullable=False, comment="Requested Quantity")
    customer_outlet = Column(Integer, ForeignKey("cust_outlet.id"), comment="Customer outlet")
    dist = Column(Float, nullable=False, comment="Distance between DC to customer")
    dist_uom = Column(Integer,  ForeignKey("uom.id"), nullable=False, comment="UOM FK")
    cust_outlet = relationship("CustomerOutlet")