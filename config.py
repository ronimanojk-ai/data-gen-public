#column no for date
date_column = 0

#Interval for timer
interval = 15

#No of hours for each time interval
period = 1

#Starting date in the format %Y-%m-%dT%H:%M:%SZ Ex: 2021-03-21T00:00:00Z (Keep null string to pick the first date)
d1 = ''#'2021-03-01T00:00:00Z'

#End date in the format %Y-%m-%dT%H:%M:%SZ Ex: 2021-03-21T00:00:00Z (Keep null string to pick the first date)
end_date = ''#'2021-03-24T00:00:00Z'

#query to be run
query = 'SELECT * from v5_test.order ORDER BY date ASC'

#producer topic name
topic = 'dettol_testing_doc_analysis'

#zookeeper date node
node = 'DATE'

#zookeeper interval node
interval_node = 'INTERVAL'

#Schema name
schema = 'v13'
