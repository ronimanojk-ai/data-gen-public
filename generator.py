#Project to simulate data into infohub

import psycopg2
import datetime as dt
from datetime import datetime
import threading
import config
import pandas as pds
from sqlalchemy import create_engine, exc
import time
import sys
import json
from sqlalchemy.orm import sessionmaker,Session,Load,load_only,join,joinedload
from sqlalchemy.sql import asc, desc, func
from model import Order, OrderItem, OrderOTIF, OrderItemDelivLoc, Customer, CustomerOutlet, DC, Location, Mill, Product, UOM, DCProductStock, DCProduct
import time
sys.path.insert(0, 'parabole-zk-system-manager/')
import globals

from kafka_utils import KafkaService, CallBack as KafkaCallBack
from zk.zookeeper_utils import Zookeeper, CallBack
import logging

FORMAT = '%(levelname)s : %(asctime)s : %(message)s : %(process)s'
logging.basicConfig(format=FORMAT,
                    datefmt='%m/%d/%Y %I:%M:%S %p', filename='api_logs.txt', filemode='w')
logger = logging.getLogger(__name__)

class KafkaProducerResponse(KafkaCallBack):
    def on_success(self, key, value=None):
        print(f'SENT SUCCESSFULLY {key}')
    def on_error(self, key, error_message):
        print(f'| ERROR | PRODUCER_RESPONSE | {error_message}')



global orders_list
global stocks_list
global d1
d1 = config.d1
global startflag
global stopflag
global interval
global increment

startflag = False
stopflag = False
interval = config.interval
increment = 0

def datetime_handler(x):
    if isinstance(x, dt.datetime):
        return x.isoformat()
    if isinstance(x, dt.timedelta):
        return str(x)
    raise TypeError("Unknown type")

class timerStartCallback(CallBack):

    def __init__(self):
        global startflag
        global increment

    def on_change(self, data, stat, node=None):
        global startflag
        global increment
        logger.info("Received start timer call")
        startflag = True
        increment = 0

class timerStopCallback(CallBack):

    def __init__(self):
        global stopflag

    def on_change(self, data, stat, node=None):
        global stopflag
        logger.info("Received stop timer call")
        stopflag = True

class timerUpdateCallback(CallBack):

    def __init__(self):
        global interval
        global increment

    def on_change(self, data, stat, node=None):
        global interval
        global increment
        increment = 0
        logger.info("Received update timer call")
        if (data == 'realtime'):
            interval = 3600
        else:
            interval = 25

def find_orders(period):
    global orders_list
    global d1
    payload = []
    msg = {}
    for index, row in enumerate(orders_list):
        curr = row.date
        logger.info("Finding Orders Between" + d1.strftime('%Y-%m-%dT%H:%M:%SZ') + " and " + curr.strftime('%Y-%m-%dT%H:%M:%SZ'))

        if curr <= d1:
            msg = row.__dict__
            msg["type"] = "order"
            items = []
            for it in row.items:
                dict = {}
                item = it.__dict__
                dict["sales_doc_item_num"] = it.sales_doc_item_num
                dict["product_name"] = it.product.name
                dict["prd_cd"] = it.product.prd_cd
                dict["uom_code"] = it.UOM.code
                dict["tot_req_quantity"] = it.tot_req_quantity
                dict["status"] = it.status
                outlets = []
                for loc in it.shipto:
                    outlet = {"name": loc.cust_outlet.name, "req_quantity": loc.req_quantity, "dist": loc.dist, "dist_uom": loc.dist_uom}
                    outlets.append(outlet)
                dict["ship_to"] = outlets
                items.append(dict)
            msg["items"] = items
            msg["customer_name"] = row.customer.name
            shipments = []

            for it in row.shipments:
                dict = {}
                dict["act_gross_wt"] = it.act_gross_wt
                dict["act_net_wt"] = it.act_net_wt
                dict["req_gross_wt"] = it.req_gross_wt
                dict["req_net_wt"] = it.req_net_wt
                dict["ship_type"] = it.ship_type
                dict["wt_uom"] = it.wt_uom
                shipments.append(dict)
            msg["shipments"] = shipments
            #msg["mill_name"] = row.mill.name
            msg["dc_name"] = row.dc.name
            msg.pop('dc')
            msg.pop('customer')
            #msg.pop('mill')
            msg.pop('_sa_instance_state')
            payload.append(msg)
        elif curr > d1:
            break
    if payload:
        logger.info(f'No of orders sent :  {len(payload)}')
        KafkaService.send(config.topic, None, json.loads(json.dumps(payload, default=datetime_handler)), KafkaProducerResponse())
        #print(json.dumps(payload, default=datetime_handler))
        orders_list = orders_list[len(payload):]
    #print(json.dumps(payload, default=datetime_handler))


def find_stocks(period):
    global stocks_list
    global d1
    msg = {}
    payload=[]

    for index, row in enumerate(stocks_list):
        curr = row.act_avail_dt
        logger.info("Finding DC Product Stocks Between" + d1.strftime('%Y-%m-%dT%H:%M:%SZ') + " and " + curr.strftime('%Y-%m-%dT%H:%M:%SZ'))

        if curr <= d1:
            msg = row.__dict__
            msg["type"] = "stock"
            msg.pop('_sa_instance_state')
            msg = {k: v for k, v in msg.items() if v is not None}
            payload.append(msg)
        elif curr > d1:
            break
    if payload:
        logger.info(f'No of stocks sent :  {len(payload)}')
        KafkaService.send(config.topic, None, json.loads(json.dumps(payload, default=datetime_handler)), KafkaProducerResponse())
        #print(json.dumps(payload, default=datetime_handler))
        stocks_list = stocks_list[len(payload):]
    #print(json.dumps(payload, default=datetime_handler)) 

def get_orders(session, d1, ascending = True):
    return session.query(Order)\
                .join(OrderItem) \
                .order_by(Order.date) \
                .filter(Order.date > d1) \
                            .all()
    #return pds.read_sql(session.query(Order).order_by(Order.date).statement, session.bind)

def get_dc_stock(session, d1, ascending = True):
    return session.query(DCProductStock) \
        .order_by(DCProductStock.act_avail_dt) \
        .filter(DCProductStock.act_avail_dt > d1)\
        .all()

def get_order_items(session, ascending = True):
    return session.query(OrderItem)\
                .order_by(OrderItem.id) \
                            .all()

if __name__ == '__main__':

    print("Initializing Zookeeper callbacks")
    Zookeeper.watch_raw_path(f'/timer/start', timerStartCallback(), append_root=True)
    Zookeeper.watch_raw_path(f'/timer/stop', timerStopCallback(), append_root=True)
    Zookeeper.watch_raw_path(f'/timer/speed', timerUpdateCallback(), append_root=True)

    while(True):
        logger.info("Waiting for timer start notification")
        time.sleep(2)
        d1 = ''
        if(startflag):
            startflag = False
            stopflag = False
            logger.info("starting the timer")
            alchemy_engine = create_engine('postgresql+psycopg2://postgres:1qaz!QAZ@10.0.2.18:5432/order_simulation',
                                           pool_recycle=300, connect_args={'options': '-csearch_path={}'.format(config.schema),
                                           'connect_timeout' : '0'} )

            logger.info("Opened database successfully")
            #dbConnection = alchemy_engine.connect();
            session = Session(alchemy_engine)
            Zookeeper.write_raw_path(config.interval_node,
                                                     interval, append_root=True)


            time.sleep(5)
            end_date = ''
            if config.end_date != '':
                end_date = datetime.strptime(config.end_date, '%Y-%m-%dT%H:%M:%SZ')
                end_date = end_date.replace(tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None))

            # Set the starting date
            if d1 == '':
                d1 = Zookeeper.read_raw_path(config.node, append_root=True)
                d1 = datetime.strptime(d1, '%Y-%m-%dT%H:%M:%SZ')
                d1 = d1.replace(tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None))
                logger.info("Initialized d1 : " + str(d1))
            else:
                d1 = datetime.strptime(config.d1, '%Y-%m-%dT%H:%M:%SZ')
                d1 = d1.replace(tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=0, name=None))

            try:
                orders_list = get_orders(session, d1)
            except:
                logger.info(f'Exception raised while getting orders')
                session = Session(alchemy_engine)
                orders_list = get_orders(session, d1)

            #stocks_list = get_dc_stock(session,d1)

            ticker = threading.Event()
            while not ticker.wait(5) and len(orders_list) > 0:
                print("increment change")
                increment = increment + 5
                if stopflag:
                    stopflag = False
                    break
                if increment == interval:
                    increment = 0
                    try:
                        if end_date != '' and d1 >= end_date:
                            break
                        session.expire_all()
                        find_orders(config.period)
                        #find_stocks(config.period)
                        if len(orders_list) > 0:
                            prev = d1
                            logger.info(f'Timer node set:  {str(d1)}')
                            Zookeeper.write_raw_path(config.node,
                                                     d1.strftime('%Y-%m-%dT%H:%M:%SZ'), append_root=True)
                            d1 = d1 + dt.timedelta(hours=config.period)
                            #if prev.day < d1.day:
                            #    print("******Day Changed******")
                            #d1 = d1 + dt.timedelta(hours=config.period)
                            logger.info(f'Initialized d1 :  {str(d1)}')
                            if stopflag:
                                stopflag = False
                                break
                    except exc.DatabaseError as e:
                        logger.info(f'Database Error raised: {e.args}')
                        session.rollback()
                    except exc.OperationalError as e:
                        logger.info(f'Database Error raised: {e.args}')
                        session.rollback()
            #session.close()
            #Session.close_all()
            logger.info("Exiting timer")


